package com.ctanli.ctanli.Models.Post

import java.io.Serializable

data class Post(var itemName: String = "",
           var uid: String = "",
           var fullName: String = "",
           var userAvatar: String = "",
           var storeLocation: String = "",
           var price: String = "",
           var desc: String = "",
           var colour: String = "",
           var timeStamp: String = "",
           var gender: String = "",
           var size: String = "",
           var qty: String = "",
           var postId: String = "",
           var category: String = "",
           var likeCount: String = "",
           var postUrl: MutableList<String> = mutableListOf<String>(),
           var likedPost: MutableList<String> = mutableListOf<String>(),
           var optToBuy : MutableList<String> = mutableListOf<String>(),
           var itemPurchasedBy : MutableList<String> = mutableListOf<String>()
        ) : Serializable



