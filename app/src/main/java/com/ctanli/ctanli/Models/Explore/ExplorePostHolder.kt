package com.ctanli.ctanli.Models.Explore

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.ctanli.ctanli.Models.Post.Post
import com.ctanli.ctanli.R
import com.squareup.picasso.Picasso

class ExplorePostHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val postImage = itemView.findViewById<ImageView>(R.id.puppyImageView)

    fun updateWithPuppy(post: Post) {
        Picasso.get().load(post.postUrl[0]).into(postImage)
    }
}