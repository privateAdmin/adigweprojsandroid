package com.ctanli.ctanli.Models.Post

import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore

class PostFactory {

    private val mFirestore = FirebaseFirestore.getInstance()
    private val TAG = "StorageActivity"

    fun fetchHomePost(callback: (ArrayList<Post>) -> Unit) {


        val home = ArrayList<Post>()
        mFirestore.collection("posts")
                .get()
                .addOnCompleteListener({ task ->
                    if (task.isSuccessful) {
                        Log.d(TAG, "my documents $task")
                        for (document in task.result) {
                            Log.d(TAG, "my docs " + document.id + " => " + document.data)
                            Log.d(TAG, "my documents $document")
                            val myObject =  document.toObject(Post::class.java)
                            home.add(myObject)
                            callback(home)
                        }
                    } else {
                        Log.d(TAG, "Error getting documents: ", task.exception)
                    }
                })
            }

    fun fetchExplorePost(callback: (ArrayList<Post>) -> Unit) {
        val explore = ArrayList<Post>()
        mFirestore.collection("posts")
                .get()
                .addOnCompleteListener({ task ->
                    if (task.isSuccessful) {
                        for (document in task.result) {
                            Log.d(TAG, "my docs " + document.id + " => " + document.data)
                            val myObject = document.toObject(Post::class.java)
                            explore.add(myObject)
                            callback(explore)
                        }
                    } else {
                        Log.d(TAG, "Error getting documents: ", task.exception)
                    }
                })
            }
        }