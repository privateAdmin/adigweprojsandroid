package com.ctanli.ctanli.Models.Post

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.ctanli.ctanli.Custom.IconRegularTextView
import com.ctanli.ctanli.Custom.IconSolidTextView
import com.ctanli.ctanli.Custom.LikePost
import com.ctanli.ctanli.Custom.TimeClass
import com.ctanli.ctanli.R
import com.google.firebase.auth.FirebaseAuth
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.post_item.view.*

class PostHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

    private val timeClass : TimeClass = TimeClass()
    private val likePost : LikePost = LikePost()

    private val postImage = itemView.findViewById<ImageView>(R.id.postImageView)
    private val postDesc = itemView.findViewById<TextView>(R.id.txtDesc)
    private val userName = itemView.findViewById<TextView>(R.id.user_Name)
    private val timeStamp = itemView.findViewById<TextView>(R.id.time_Stamp)
    private val userAvatar = itemView.findViewById<ImageView>(R.id.user_Avatar)
    private val likeCount = itemView.findViewById<TextView>(R.id.likeNum)
    private val comment = itemView.findViewById<IconRegularTextView>(R.id.comment_button)
    private val more = itemView.findViewById<IconSolidTextView>(R.id.more_button)
    private val like = itemView.findViewById<IconRegularTextView>(R.id.like_button)
    private val likeSolid = itemView.findViewById<IconSolidTextView>(R.id.like_Solid_button)
    private val itemState = itemView.findViewById<LinearLayout>(R.id.check_Item_State)

    private val red = itemView.findViewById<ImageView>(R.id.home_red)
    private val yellow = itemView.findViewById<ImageView>(R.id.home_yellow)
    private val green = itemView.findViewById<ImageView>(R.id.home_green)

    override fun onClick(p0: View?) {
        Toast.makeText(itemView.context, "clicked", Toast.LENGTH_LONG).show()
    }

    fun updateWithPuppy(post: Post,
                        listener: (Post) -> Unit,
                        itemStateListener: (Post) -> Unit,
                        moreListener: (Post) -> Unit)
            = with(itemView) {
        timeStamp.text = timeClass.getTimeAgo(post.timeStamp.toLong())
        Picasso.get().load(post.userAvatar).into(userAvatar)
        Picasso.get().load(post.postUrl[0]).into(postImage)
        userName.text = post.fullName
        postDesc.text = post.desc
        likeCount.text = post.likeCount

        if (post.likedPost.contains((FirebaseAuth.getInstance().currentUser!!.uid))) {
            like.visibility = View.GONE
            likeSolid.visibility = View.VISIBLE
        }

        itemView.setOnClickListener {
            listener(post)
        }

        itemState.setOnClickListener {
            itemStateListener(post)
        }

        more.setOnClickListener {
            moreListener(post)
        }

        itemView.user_Avatar.setOnClickListener {
            Toast.makeText(itemView.context, "userAvatar clicked and worked", Toast.LENGTH_SHORT).show()
        }

        itemView.like_button.setOnClickListener {
            like.visibility = View.GONE
            likeSolid.visibility = View.VISIBLE
            likePost.likePost(post, { likeCounts ->
                likeCount.text = likeCounts
            })
        }

        itemView.like_Solid_button.setOnClickListener {
            like.visibility = View.VISIBLE
            likeSolid.visibility = View.GONE
            likePost.unLikPost(post, { likeCounts ->
                likeCount.text = likeCounts
            })
        }

        if ((post.itemPurchasedBy.isEmpty()) && (post.optToBuy.isEmpty())) {
            green.setBackgroundColor(ContextCompat.getColor(context, R.color.green))
            red.setBackgroundColor(ContextCompat.getColor(context, R.color.red50))
            yellow.setBackgroundColor(ContextCompat.getColor(context, R.color.yellow50))
            return
        }

        post.optToBuy.count().let { value ->
            if (post.uid == FirebaseAuth.getInstance().currentUser!!.uid) {
                return
            }
            if (value >= 1) {
                yellow.setBackgroundColor(ContextCompat.getColor(context, R.color.yellow))
                red.setBackgroundColor(ContextCompat.getColor(context, R.color.red50))
                green.setBackgroundColor(ContextCompat.getColor(context, R.color.green50))
            }
        }

        post.itemPurchasedBy.count().let { value ->
            if (post.uid == FirebaseAuth.getInstance().currentUser!!.uid) {
                return
            }
            if (value >= 1) {
                red.setBackgroundColor(ContextCompat.getColor(context, R.color.red))
                yellow.setBackgroundColor(ContextCompat.getColor(context, R.color.yellow50))
                green.setBackgroundColor(ContextCompat.getColor(context, R.color.green50))
            }
        }
    }
}


