package com.ctanli.ctanli.Models.Post

enum class PostListType {
    Home,
    Explore
}