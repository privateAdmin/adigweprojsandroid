package com.ctanli.ctanli.Activities.DetailPageContent

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.Toast
import com.ctanli.ctanli.R
import com.squareup.picasso.Picasso

class ImageViewPagerAdapter : PagerAdapter {

    private var con: Context
    private var pathURL: MutableList<String>
    lateinit var inflator: LayoutInflater

    constructor(con: Context, imageURL: MutableList<String>) : super() {
        this.con = con
        this.pathURL = imageURL
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object` as RelativeLayout
    }

    override fun getCount(): Int {
        return pathURL.count()
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        var image: ImageView
        inflator = con.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var rv: View = inflator.inflate(R.layout.swipe_image_fragment, container, false)
        image = rv.findViewById(R.id.iv_image) as ImageView
        Picasso.get().load(pathURL[position]).into(image)
        container.addView(rv)
        image.setOnClickListener {
            Toast.makeText(con, "clicked and worked", Toast.LENGTH_SHORT).show()
        }
        return rv
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }
}