package com.ctanli.ctanli.Activities.DetailPageContent

import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.ctanli.ctanli.Models.Post.Post
import com.ctanli.ctanli.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.rd.PageIndicatorView
import com.squareup.picasso.Picasso
import org.jetbrains.anko.alert
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.noButton
import org.jetbrains.anko.yesButton
import java.util.*

class DetailActivity : AppCompatActivity() {

    private var userIdentifier: String = ""
    private var postIdentifier: String = ""
    private var hasOptToBuy: Boolean? = null
    private var itemNotAvailable: Boolean? = null
    private val mFirestore = FirebaseFirestore.getInstance()

    lateinit var mPager: ViewPager
    private lateinit var name: TextView
    private lateinit var userAvatar: ImageView
    private lateinit var categoryName: TextView
    private lateinit var itemName: TextView
    private lateinit var qtyValue: TextView
    private lateinit var sizeValue: TextView
    private lateinit var genderValue: TextView
    private lateinit var storeLocation: TextView
    private lateinit var colorValue: TextView
    private lateinit var priceValue: TextView
    private lateinit var description: TextView

    private lateinit var red: ImageView
    private lateinit var yellow: ImageView
    private lateinit var green: ImageView


    private lateinit var sview: RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        sview = findViewById(R.id.relative)

        userAvatar = findViewById(R.id.user_Photo)
        name = findViewById(R.id.user_Name)
        priceValue = findViewById(R.id.price_value)
        qtyValue = findViewById(R.id.qty_value)
        colorValue = findViewById(R.id.color_value)
        genderValue = findViewById(R.id.gender_value)
        sizeValue = findViewById(R.id.size_value)
        storeLocation = findViewById(R.id.store_url)
        description = findViewById(R.id.description)
        itemName = findViewById(R.id.txtName)
        categoryName = findViewById(R.id.cat_Name)

        red = findViewById(R.id.red_image)
        yellow = findViewById(R.id.yellow_Image)
        green = findViewById(R.id.green_Image)

        var post = intent.extras.get("value") as Post
        val imageURL = post.postUrl

        userIdentifier = post.uid
        postIdentifier = post.postId

        mPager = findViewById<View>(R.id.viewpager) as ViewPager
        var adapter: PagerAdapter = ImageViewPagerAdapter(this, imageURL)
        mPager.adapter = adapter

        //dots
        val pageIndicatorView: PageIndicatorView = findViewById(R.id.pageIndicatorView)
        pageIndicatorView.setViewPager(mPager)

        Picasso.get().load(post.userAvatar).into(userAvatar)
        name.text = post.fullName
        priceValue.text = post.price
        qtyValue.text = post.qty
        genderValue.text = post.gender
        colorValue.text = post.colour
        sizeValue.text = post.size
        storeLocation.text = post.storeLocation
        description.text = post.desc
        itemName.text = post.itemName
        categoryName.text = post.category

        mPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                Toast.makeText(this@DetailActivity, "clicked and worked", Toast.LENGTH_SHORT).show()
            }
        })

        red.setOnClickListener { view ->
            ifItemAvailable()
        }

        yellow.setOnClickListener { view ->
            handleOptionToBuy()
        }

        green.setOnClickListener { view ->
            checkGreenState()
        }
    }

    private fun ifItemAvailable() {
        if (userIdentifier == FirebaseAuth.getInstance().currentUser!!.uid) {
            alert("psych! , Sorry it's a surprise") {
                title = "Ooops"
            }.show()
            return
        }
        alert("Confirm item is purchased.", "Welcome") {
            yesButton {
                itemPurchasedBy()
            }
            noButton {}
        }.show()
    }

    private fun checkGreenState() {

        if (userIdentifier == FirebaseAuth.getInstance().currentUser!!.uid) {
            alert("psych! , Sorry it's a surprise") {
                title = "Ooops"
            }.show()
            return
        }

        mFirestore.collection("posts").document(postIdentifier).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val value = task.result.toObject(Post::class.java)
                itemNotAvailable = true
                if (!value.itemPurchasedBy.isEmpty()) {
                    alert("Item purchased, Thanks.") {
                        title = "Sorry"
                    }.show()
                } else {
                    alert("Item Still available, Thanks.") {
                        title = ""
                    }.show()
                }
            }
        }
    }

    private fun handleOptionToBuy() {
        val opt = HashMap<String, Any>()
        var otpToBuy: MutableList<String> = mutableListOf<String>()
        otpToBuy.add((FirebaseAuth.getInstance().currentUser!!.uid))

        hasOptToBuy = false
        mFirestore.collection("posts").document(postIdentifier).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val value = task.result.toObject(Post::class.java)
                value.optToBuy.forEach { v ->
                    if (v == (FirebaseAuth.getInstance().currentUser!!.uid)) {
                        //opt out
                        alert("Do you want to opt out of this item?", "Opting Out?") {
                            yesButton {
                                value.optToBuy.remove((FirebaseAuth.getInstance().currentUser!!.uid))
                                hasOptToBuy = true
                                if (hasOptToBuy == true) {
                                    opt["optToBuy"] = value.optToBuy
                                    mFirestore.collection("posts").document(postIdentifier).update(opt as Map<String, Any>).addOnCompleteListener { task ->
                                        if (task.isSuccessful) {
                                            snackbar(sview, "You just opted out of purchasing this item")
                                        }
                                    }
                                }
                            }
                            noButton {}
                        }.show()
                        return@addOnCompleteListener
                    }
                }

                for (like in value.optToBuy) {
                    otpToBuy.add(like)
                    hasOptToBuy = true
                }

                if (value.optToBuy.isEmpty()) {
                    hasOptToBuy = true
                }

                if (hasOptToBuy == true) {
                    opt["optToBuy"] = otpToBuy
                    mFirestore.collection("posts").document(postIdentifier).update(opt as Map<String, Any>).addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            snackbar(sview, "You just opted to purchase this item")
                        }
                    }
                }
            } else {
            }
        }
    }

    fun itemPurchasedBy() {

        val notAvailable = HashMap<String, Any>()
        var purchasedBy: MutableList<String> = mutableListOf<String>()
        purchasedBy.add((FirebaseAuth.getInstance().currentUser!!.uid))

        itemNotAvailable = false
        mFirestore.collection("posts").document(postIdentifier).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val value = task.result.toObject(Post::class.java)
                itemNotAvailable = true
                if (!value.itemPurchasedBy.isEmpty()) {
                    value.itemPurchasedBy.forEach { v ->
                        if (v == (FirebaseAuth.getInstance().currentUser!!.uid)) {
                            alert("You have already marked this item purchased, Do you want to cancel?.", "Welcome") {
                                yesButton {

                                }
                                noButton {

                                }
                            }.show()
                            return@addOnCompleteListener
                        }
                    }

                    alert("Item purchased, Thanks.") {
                        title = "Sorry"
                    }.show()
                    itemNotAvailable = false
                    return@addOnCompleteListener
                }

                if (itemNotAvailable == true) {
                    notAvailable["itemPurchasedBy"] = purchasedBy
                    mFirestore.collection("posts").document(postIdentifier).update(notAvailable as Map<String, Any>).addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            snackbar(sview, "You have confirmed this item has been purchase.")
                        }
                    }
                }
            } else {}
        }
    }
}
