package com.ctanli.ctanli.Activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.*
import com.ctanli.ctanli.R
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.GlideEngine
import com.zhihu.matisse.internal.entity.CaptureStrategy
import java.io.ByteArrayOutputStream
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class SignUpActivity : AppCompatActivity() {

    //Get Firebase auth instance

    private val TAG = "StorageActivity"
    private var userIdentifier: String = ""
    private var bitmap: Bitmap? = null
    var auth = FirebaseAuth.getInstance()
    val mFirestore: FirebaseFirestore = FirebaseFirestore.getInstance()
    private var imageReference: StorageReference? = null

    private lateinit var btnSignUp: Button
    private lateinit var progressBar: ProgressBar
    private lateinit var clickMe: ImageView
    private lateinit var username: EditText
    private lateinit var fullName: EditText
    private lateinit var inputEmail: EditText
    private lateinit var current: LocalDateTime
    private lateinit var formatter: DateTimeFormatter
    private lateinit var formatted: String

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        getPermissionToReadMedia()
        imageReference = FirebaseStorage.getInstance().reference.child("userAvatar")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            current = LocalDateTime.now()
            formatter = DateTimeFormatter.BASIC_ISO_DATE
            formatted = current.format(formatter)
            Log.d("Time", "Current time: $formatted")
        }

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        //setSupportActionBar(toolbar)


        var inputPassword = findViewById<EditText>(R.id.password)
        var btnLogin = findViewById<Button>(R.id.sign_in_button)
        var btnReset = findViewById<Button>(R.id.btn_reset_password)
        progressBar = findViewById(R.id.progressBar)
        btnSignUp = findViewById(R.id.sign_up_button)
        username = findViewById(R.id.username)
        fullName = findViewById(R.id.full_name)
        inputEmail = findViewById(R.id.email)

        // get reference to ImageView
        clickMe = findViewById<ImageView>(R.id.image_click)
        // set on-click listener
        clickMe.setOnClickListener {
            // your code to perform when the user clicks on the ImageView
            //Toast.makeText(this@UploadContentActivity, "You clicked on ImageView.", Toast.LENGTH_SHORT).show()
            AsyncTask.execute {
                //TODO your background code
                Matisse.from(this@SignUpActivity)
                        .choose(MimeType.allOf())
                        .theme(R.style.Matisse_Dracula)
                        .countable(false)
                        .capture(true)
                        .captureStrategy(CaptureStrategy(true, "com.zhihu.matisse.sample.fileprovider"))
                        .maxSelectable(1)
                        .imageEngine(GlideEngine())
                        .gridExpectedSize(resources.getDimensionPixelSize(R.dimen.grid_expected_size))
                        .thumbnailScale(0.85f)
                        .forResult(23)
            }
        }

        btnLogin.setOnClickListener { view ->
            startActivity(Intent(this@SignUpActivity, LoginActivity::class.java))
        }

        btnReset.setOnClickListener { view ->
            startActivity(Intent(this@SignUpActivity, ResetPasswordActivity::class.java))
        }

        btnSignUp.setOnClickListener { view ->

            val email = inputEmail.text.toString().trim()
            val password = inputPassword.text.toString().trim()

            if (TextUtils.isEmpty(email)) {
                Toast.makeText(applicationContext, "Enter email address!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (TextUtils.isEmpty(password)) {
                Toast.makeText(applicationContext, "Enter password!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (password.length < 6) {
                Toast.makeText(applicationContext, "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            progressBar.visibility = View.VISIBLE

            auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, OnCompleteListener<AuthResult> { task ->
                Toast.makeText(this@SignUpActivity, "createUserWithEmail:onComplete:" + task.isSuccessful, Toast.LENGTH_SHORT).show()
                progressBar.visibility = View.GONE

                if (!task.isSuccessful) {
                    Toast.makeText(this@SignUpActivity, "Authentication failed." + task.exception!!,
                            Toast.LENGTH_SHORT).show()
                } else {
                    userIdentifier = task.result.user.uid
                    uploadBytes()
                    startActivity(Intent(this@SignUpActivity, MainActivity::class.java))
                    finish()
                }
            })
        }
    }

    private val REQUEST_CODE_CHOOSE = 23
    var mSelected = ArrayList<Uri>()

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
            mSelected = Matisse.obtainResult(data) as ArrayList<Uri>

            Log.d("Success", "user selected image: $mSelected $data")
            bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, mSelected[0])
            Log.d("Success", "user selected image: $bitmap")
            clickMe.setImageBitmap(bitmap)
            val bitma = arrayOf<Bitmap>()

            Toast.makeText(this, "Image selected or taken", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Dismiss view", Toast.LENGTH_SHORT).show()
            Log.d("Failed", "dismiss: $resultCode $requestCode $data")
        }
    }

    private fun uploadBytes() {
        if (mSelected != null) {
            val fileName = userIdentifier

            val baos = ByteArrayOutputStream()
            bitmap!!.compress(Bitmap.CompressFormat.JPEG, 50, baos)
            val data: ByteArray = baos.toByteArray()

            Log.e(TAG, "check if file vail: " + mSelected[0])
            val fileRef = imageReference!!.child(fileName).child(UUID.randomUUID().toString())

            fileRef.putBytes(data).addOnSuccessListener { taskSnapshot ->
                        Log.e(TAG, "Uri: " + taskSnapshot.downloadUrl.toString())
                        Log.e(TAG, "Name: " + taskSnapshot.metadata!!.name)
                        Log.d("", "taskSnapshot: " + taskSnapshot.metadata!!.path + " - " + taskSnapshot.metadata!!.sizeBytes / 1024 + " KBs")
                        Toast.makeText(this, "File Uploaded ", Toast.LENGTH_LONG).show()
                FirebaseAuth.getInstance().currentUser!!.updateProfile(
                        UserProfileChangeRequest.Builder()
                                .setDisplayName(fullName.text.toString())
                                .setPhotoUri(taskSnapshot.downloadUrl)
                                .build()
                )

                updateUserInfo(userIdentifier, username.text.toString(), fullName.text.toString(), formatted, taskSnapshot.downloadUrl.toString(), inputEmail.text.toString())
                    }.addOnFailureListener { exception ->
                        Toast.makeText(this, exception.message, Toast.LENGTH_LONG).show()
                    }.addOnProgressListener { taskSnapshot ->
                        // progress percentage
                        val progress = 100.0 * taskSnapshot.bytesTransferred / taskSnapshot.totalByteCount

                        // percentage in progress dialog
                        val intProgress = progress.toInt()

                    }.addOnPausedListener { System.out.println("Upload is paused!") }

        } else {
            Toast.makeText(this, "No File!", Toast.LENGTH_LONG).show()
        }
    }

    private fun getFileExtension(uri: Uri): String {
        val contentResolver = contentResolver
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(contentResolver.getType(uri))
    }

    private fun updateUserInfo(userIdentifier: String, username: String, fullname: String, timeStamp: String, userAvatar: String, email: String) {
        val items = HashMap<String, Any>()
        items["uid"] = userIdentifier
        items["username"] = username
        items["fullName"] = fullname
        items["timeStamp"] = timeStamp
        items["userAvatar"] = userAvatar
        items["email"] = email

        mFirestore.collection("users").document(userIdentifier).set(items).addOnSuccessListener {
            Toast.makeText(this, "Data Stored", Toast.LENGTH_SHORT).show()
        }.addOnFailureListener {
            Toast.makeText(this, "Data Not Stored", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onResume() {
        super.onResume()
        progressBar.visibility = View.GONE
    }

    // Identifier for the permission request
    private val READ_EXTERNAL_STORAGE_PERMISSIONS_REQUEST = 1

    @RequiresApi(Build.VERSION_CODES.M)
    // Called when the user is performing an action which requires the app to read the
    private fun getPermissionToReadMedia() {
        val listPermissionsNeeded = ArrayList<String>()

        val cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val storagePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        val wStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }

        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }

        if (wStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(arrayOf(listPermissionsNeeded.size.toString())), READ_EXTERNAL_STORAGE_PERMISSIONS_REQUEST)
        } else {
            Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show()
        }

    }

    // Callback with the request from calling requestPermissions(...)
    @SuppressLint("NewApi")
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == READ_EXTERNAL_STORAGE_PERMISSIONS_REQUEST) {
            when {
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED -> {
                    Toast.makeText(this,
                            "FlagUp Requires Access to Camera.", Toast.LENGTH_SHORT)
                            .show()
                    finish()
                }
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED -> {
                    Toast.makeText(this,
                            "FlagUp Requires Access to Read Storage.",
                            Toast.LENGTH_SHORT).show()
                    finish()
                }
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED -> {
                    Toast.makeText(this,
                            "FlagUp Requires Access to Write Storage.",
                            Toast.LENGTH_SHORT).show()
                    finish()
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }
}