package com.ctanli.ctanli.Activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.annotation.RequiresApi
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialog
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.*
import com.ctanli.ctanli.Models.Post.Post
import com.ctanli.ctanli.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.GlideEngine
import com.zhihu.matisse.internal.entity.CaptureStrategy
import kotlinx.android.synthetic.main.activity_upload_content.*
import org.jetbrains.anko.indeterminateProgressDialog
import java.io.ByteArrayOutputStream
import java.util.*

class UploadContentActivity : AppCompatActivity() {

    private lateinit var clickMe: ImageView
    private lateinit var name: EditText
    private lateinit var colour: EditText
    private lateinit var price: EditText
    private lateinit var qty: EditText
    private lateinit var size: EditText
    private lateinit var gender: EditText
    private lateinit var storeUrl: EditText
    private lateinit var desc: EditText
    private lateinit var selectCat: Button
    private lateinit var sheetView: LinearLayout
    private lateinit var bday: View
    private lateinit var other: View
    private lateinit var mBottomSheetDialog: BottomSheetDialog

    private var bitmapArray: MutableList<Bitmap> = mutableListOf<Bitmap>()
    private var imageStringArray: MutableList<String> = mutableListOf<String>()
    private val mFirestore: FirebaseFirestore = FirebaseFirestore.getInstance()
    private var imageReference: StorageReference? = null
    private lateinit var formatted: String

    private var postIdentifier: String = ""
    private var userIdentifier: String = ""
    private var fulName: String = ""
    private var userAvatar: String = ""
    private var uploadCount: Int = 0

    val TAG = "StorageActivity"

    @SuppressLint("InflateParams")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_content)
        imageReference = FirebaseStorage.getInstance().reference.child("postImages")
        getPermissionToReadUserContacts()

        fulName = FirebaseAuth.getInstance().currentUser!!.displayName!!
        userAvatar = FirebaseAuth.getInstance().currentUser!!.photoUrl.toString()
        userIdentifier = FirebaseAuth.getInstance().currentUser!!.uid

        // get reference to ImageView
        clickMe = findViewById(R.id.clickMe)
        selectCat = findViewById(R.id.category)
        name = findViewById(R.id.name)
        price = findViewById(R.id.price)
        qty = findViewById(R.id.qty)
        colour = findViewById(R.id.color)
        gender = findViewById(R.id.gender)
        size = findViewById(R.id.size)
        storeUrl = findViewById(R.id.link)
        desc = findViewById(R.id.desc)
        sheetView = layoutInflater.inflate(R.layout.bottom_sheet, null) as LinearLayout
        mBottomSheetDialog = BottomSheetDialog(this)

        val tsLong = System.currentTimeMillis() / 1000
        formatted = tsLong.toString()

        selectCat.setOnClickListener { view ->
            mBottomSheetDialog.setContentView(sheetView)
            mBottomSheetDialog.setCancelable(false)
            mBottomSheetDialog.setCanceledOnTouchOutside(true)
            mBottomSheetDialog.show()
        }

        mBottomSheetDialog.setOnShowListener {

            bday = sheetView.findViewById<View>(R.id.fragment_history_bottom_sheet_birthday)
            bday.setOnClickListener {
                selectCat.text = "BIRTHDAY"
                mBottomSheetDialog.dismiss()
            }

            other = sheetView.findViewById<View>(R.id.fragment_history_bottom_sheet_other)
            other.setOnClickListener {
                selectCat.text = "OTHER"
                mBottomSheetDialog.dismiss()
            }
        }

        mBottomSheetDialog.setOnDismissListener {
            if (sheetView.parent != null) {
                var parent = sheetView.parent as ViewGroup
                    parent.removeView(sheetView)
                }
            }

        // set on-click listener
        clickMe.setOnClickListener {
            // your code to perform when the user clicks on the ImageView
            //Toast.makeText(this@UploadContentActivity, "You clicked on ImageView.", Toast.LENGTH_SHORT).show()
            AsyncTask.execute {
                //TODO your background code
                Matisse.from(this@UploadContentActivity)
                        .choose(MimeType.allOf())
                        .theme(R.style.Matisse_Dracula)
                        .countable(false)
                        .capture(true)
                        .captureStrategy(CaptureStrategy(true, "com.zhihu.matisse.sample.fileprovider"))
                        .maxSelectable(4)
                        .imageEngine(GlideEngine())
                        .gridExpectedSize(resources.getDimensionPixelSize(R.dimen.grid_expected_size))
                        .thumbnailScale(0.85f)
                        .forResult(23)
            }
        }

        if (editIsClicked()) {
            var post = intent.extras.get("editpost") as Post
            Toast.makeText(this, "Edit button is clicked $post", Toast.LENGTH_LONG).show()

            postIdentifier = post.postId
            selectCat.text = post.category
            name.setText(post.itemName)
            price.setText(post.price)
            qty.setText(post.qty)
            colour.setText(post.colour)
            gender.setText(post.gender)
            size.setText(post.size)
            storeUrl.setText(post.storeLocation)
            desc.setText(post.desc)
            Picasso.get().load(post.postUrl[0]).into(clickMe)
        }
    }

    private fun uploadBytes() {
        if (mSelected != null) {
            val fileName = FirebaseAuth.getInstance().currentUser!!.uid

            for (bit in bitmapArray) {
                Log.e(TAG, "Uri bit:  $bit")
                val baos = ByteArrayOutputStream()
                bit.compress(Bitmap.CompressFormat.JPEG, 50, baos)
                val data: ByteArray = baos.toByteArray()

                val fileRef = imageReference!!.child(fileName).child(UUID.randomUUID().toString())
                fileRef.putBytes(data).addOnSuccessListener { taskSnapshot ->
                    Log.e(TAG, "Uri: " + taskSnapshot.downloadUrl.toString())
                    Log.e(TAG, "Name: " + taskSnapshot.metadata!!.name)
                    Log.d("", "taskSnapshot: " + taskSnapshot.metadata!!.path + " - " + taskSnapshot.metadata!!.sizeBytes / 1024 + " KBs")
                    Toast.makeText(this, "File Uploaded ", Toast.LENGTH_LONG).show()

                     taskSnapshot.downloadUrl.toString().let { image ->
                         imageStringArray.add(image)
                         uploadCount++
                     }

                    if (uploadCount == bitmapArray.count()) {
                        //indeterminateProgressDialog("Uploading...").show()
                        uploadContentToFireStore()
                    }

                }.addOnFailureListener { exception ->
                    Toast.makeText(this, exception.message, Toast.LENGTH_LONG).show()
                }.addOnProgressListener { taskSnapshot ->
                    // progress percentage
                    val progress = 100.0 * taskSnapshot.bytesTransferred / taskSnapshot.totalByteCount

                    // percentage in progress dialog
                    val intProgress = progress.toInt()

                }.addOnPausedListener {
                    System.out.println("Upload is paused!")
                }
            }
        } else {
            Toast.makeText(this, "No File!", Toast.LENGTH_LONG).show()
        }
    }

    private fun getFileExtension(uri: Uri): String {
        val contentResolver = contentResolver
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(contentResolver.getType(uri))
    }

    fun editIsClicked(): Boolean {
        if (intent != null) {
            if (intent.extras != null) {
                return intent.extras.getBoolean("isEditClicked")
            }
        }
        return false
    }


    private fun uploadContentToFireStore() {

        var myList: MutableList<String> = mutableListOf<String>()
        val ref = mFirestore.collection("posts").document()
        val postId = ref.id

        val items = HashMap<String, Any>()
        items["uid"] = userIdentifier
        items["fullName"] = fulName
        items["userAvatar"] = userAvatar
        items["itemName"] = name.text.toString()
        items["price"] = price.text.toString()
        items["qty"] = qty.text.toString()
        items["size"] = size.text.toString()
        items["storeLocation"] = storeUrl.text.toString()
        items["desc"] = desc.text.toString()
        items["colour"] = colour.text.toString()
        items["timeStamp"] = formatted
        items["gender"] = gender.text.toString()
        items["postId"] = postId
        items["postUrl"] = imageStringArray
        items["category"] = selectCat.text
        items["likeCount"] = "0"
        items["likedPost"] = myList

        mFirestore.collection("posts").document(postId).set(items).addOnSuccessListener {
            Toast.makeText(this, "Data Stored", Toast.LENGTH_SHORT).show()
        }.addOnFailureListener {
            Toast.makeText(this, "Data Not Stored", Toast.LENGTH_SHORT).show()
            //indeterminateProgressDialog("Uploading...").dismiss()
            //this.finish()
        }
    }

    private fun uploadEditedContentToFireStore() {
        val items = HashMap<String, Any>()
        items["itemName"] = name.text.toString()
        items["price"] = price.text.toString()
        items["qty"] = qty.text.toString()
        items["size"] = size.text.toString()
        items["storeLocation"] = storeUrl.text.toString()
        items["desc"] = desc.text.toString()
        items["colour"] = colour.text.toString()
        items["timeStamp"] = formatted
        items["gender"] = gender.text.toString()
        items["category"] = selectCat.text

        mFirestore.collection("posts").document(postIdentifier).update(items).addOnSuccessListener {
            Toast.makeText(this, "Data Stored", Toast.LENGTH_SHORT).show()
        }.addOnFailureListener {
            Toast.makeText(this, "Data Not Stored", Toast.LENGTH_SHORT).show()
            //indeterminateProgressDialog("Uploading...").dismiss()
            //this.finish()
        }
    }

    private val REQUEST_CODE_CHOOSE = 23
    var mSelected = ArrayList<Uri>()

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
            mSelected = Matisse.obtainResult(data) as ArrayList<Uri>
            Log.d("Failed", "user selected image: $mSelected $data")

            for (uri in mSelected) {
                //myList.add(i)
                var bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
                bitmapArray.add(bitmap)
                Log.d("Failed", "user selected bitmapArray: $bitmapArray")
            }

            clickMe.setImageBitmap(bitmapArray[0])

            Toast.makeText(this, "Image selected or taken", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Dismiss view", Toast.LENGTH_SHORT).show()
            Log.d("Failed", "dismiss: $resultCode $requestCode $data")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.post_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        if (editIsClicked()) {
            uploadEditedContentToFireStore()
        } else {
            uploadBytes()
        }

        val id = item.itemId

        if (id == R.id.action_post) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    // Identifier for the permission request
    private val READ_EXTERNAL_STORAGE_PERMISSIONS_REQUEST = 1

    @RequiresApi(Build.VERSION_CODES.M)
    // Called when the user is performing an action which requires the app to read the
    private fun getPermissionToReadUserContacts() {
        val listPermissionsNeeded = ArrayList<String>()

        val cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val storagePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        val wStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }

        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }

        if (wStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(arrayOf(listPermissionsNeeded.size.toString())), READ_EXTERNAL_STORAGE_PERMISSIONS_REQUEST)
        } else {
            Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show()
        }

    }

    // Callback with the request from calling requestPermissions(...)
    @SuppressLint("NewApi")
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == READ_EXTERNAL_STORAGE_PERMISSIONS_REQUEST) {
            when {
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED -> {
                    Toast.makeText(this,
                            "FlagUp Requires Access to Camera.", Toast.LENGTH_SHORT)
                            .show()
                    finish()
                }
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED -> {
                    Toast.makeText(this,
                            "FlagUp Requires Access to Read Storage.",
                            Toast.LENGTH_SHORT).show()
                    finish()
                }
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED -> {
                    Toast.makeText(this,
                            "FlagUp Requires Access to Write Storage.",
                            Toast.LENGTH_SHORT).show()
                    finish()
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }
}
