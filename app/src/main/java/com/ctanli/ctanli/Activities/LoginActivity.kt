package com.ctanli.ctanli.Activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.ctanli.ctanli.R
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import org.jetbrains.anko.indeterminateProgressDialog

class LoginActivity : AppCompatActivity() {

    var auth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (auth?.currentUser != null) {
            startActivity(Intent(this@LoginActivity, MainActivity::class.java))
            finish()
        }

        setContentView(R.layout.activity_login)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        //setSupportActionBar(toolbar)

        var inputEmail = findViewById<EditText>(R.id.email)
        var inputPassword = findViewById<EditText>(R.id.password)
        //var progressBar = findViewById<ProgressBar>(R.id.progressBar)
        var btnSignUp = findViewById<Button>(R.id.btn_signup)
        var btnLogin = findViewById<Button>(R.id.btn_login)
        var btnReset = findViewById<Button>(R.id.btn_reset_password)

        btnSignUp.setOnClickListener { view ->
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }

        btnReset.setOnClickListener { view ->
            startActivity(Intent(this@LoginActivity, ResetPasswordActivity::class.java))
        }

        btnLogin.setOnClickListener { view ->
            val email = inputEmail.text.toString()
            val password = inputPassword.text.toString()

            if (TextUtils.isEmpty(email)) {
                Toast.makeText(applicationContext, "Enter email address!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (TextUtils.isEmpty(password)) {
                Toast.makeText(applicationContext, "Enter password!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            indeterminateProgressDialog("Logging In...").show()

            auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, OnCompleteListener<AuthResult> { task ->
                if(task.isSuccessful){
                    var intent = Intent(this, MainActivity::class.java)
                    intent.putExtra("id", auth.currentUser?.email)
                    indeterminateProgressDialog("Uploading...").dismiss()
                    startActivity(intent)
                } else {
                    if (password.length < 6) {
                        inputPassword.error = getString(R.string.minimum_password)
                    } else {
                        Toast.makeText(this@LoginActivity, getString(R.string.auth_failed), Toast.LENGTH_LONG).show()
                    }
                }
            })
        }
    }
}
