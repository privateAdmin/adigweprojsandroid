package com.ctanli.ctanli.Fragments

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ctanli.ctanli.Adapters.ExploreAdapter
import com.ctanli.ctanli.Models.Post.PostFactory
import com.ctanli.ctanli.Models.Post.PostListType
import com.ctanli.ctanli.R


@SuppressLint("ValidFragment")
class ExploreFragment(context: Context) : Fragment() {

    val postFactory : PostFactory = PostFactory()
    val ARG_GRID_TYPE = "GRID_TYPE"
    val passThroughContext : Context = context


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_explore, container, false)
        val recyclerView = rootView.findViewById<RecyclerView>(R.id.recyclerView)
        val listType = this.arguments?.getSerializable(ARG_GRID_TYPE) as PostListType

        when (listType) {
            PostListType.Explore -> postFactory.fetchExplorePost { post ->
                recyclerView.adapter = ExploreAdapter(post)
            }
        }
        recyclerView.layoutManager = GridLayoutManager(passThroughContext,3)
        return rootView
    }

    companion object {
        val ARG_GRID_TYPE = "GRID_TYPE"

        fun newInstance(listType: PostListType, context: Context): ExploreFragment {
            val fragment = ExploreFragment(context)
            val args = Bundle()
            args.putSerializable(ARG_GRID_TYPE, listType)
            fragment.arguments = args
            return fragment
        }
    }
}