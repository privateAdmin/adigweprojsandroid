package com.ctanli.ctanli.Fragments

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ctanli.ctanli.Activities.DetailPageContent.DetailActivity
import com.ctanli.ctanli.Activities.MainActivity
import com.ctanli.ctanli.Activities.UploadContentActivity
import com.ctanli.ctanli.Adapters.PostAdapter
import com.ctanli.ctanli.Models.Post.PostFactory
import com.ctanli.ctanli.Models.Post.PostListType
import com.ctanli.ctanli.R
import android.app.AlertDialog
import android.support.design.widget.BottomSheetDialog
import android.widget.*
import com.ctanli.ctanli.Models.Post.Post
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import android.net.Uri

@SuppressLint("ValidFragment")
class HomeFragment(passedContext: Context) : Fragment() {

    private lateinit var posts: Post
    private lateinit var mPost: ArrayList<Post>

    private lateinit var sheetView: LinearLayout
    private lateinit var mBottomSheetDialog: BottomSheetDialog
    private val postFactory : PostFactory = PostFactory()
    private val ARG_LIST_TYPE = "LIST_TYPE"
    private val passThroughContext : Context = passedContext
    private lateinit var redText: TextView
    private lateinit var yellowText: TextView
    private lateinit var greenText: TextView
    private lateinit var redImage: ImageView
    private lateinit var yellowImage: ImageView
    private lateinit var greenImage: ImageView

    private lateinit var share: View
    private lateinit var edit: View
    private lateinit var delete: View

    private val mFirestore: FirebaseFirestore = FirebaseFirestore.getInstance()
    private val mFirebaseStorage = FirebaseStorage.getInstance()

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        sheetView = layoutInflater.inflate(R.layout.more_button_bottom_sheet, null) as LinearLayout
        mBottomSheetDialog = BottomSheetDialog(passThroughContext)

        val rootView = inflater.inflate(R.layout.fragment_home, container, false)
        val recyclerView = rootView.findViewById<RecyclerView>(R.id.puppyRecyclerView)

        val act = activity as MainActivity?
        if (act != null) {
            val fab = act.findViewById<View>(R.id.fab)
            fab.setOnClickListener {
                val intent = Intent(context, UploadContentActivity::class.java)
                startActivity(intent)
            }
        }

        val listType = this.arguments?.getSerializable(ARG_LIST_TYPE) as PostListType
        when (listType) {
            PostListType.Home -> postFactory.fetchHomePost { post ->
                recyclerView.adapter = PostAdapter(
                        post, { itemPost ->
                            val intent = Intent(activity, DetailActivity::class.java)
                            val bundle = Bundle()
                            bundle.putSerializable("value", itemPost)
                            intent.putExtras(bundle)
                            startActivity(intent)
                        },
                    { itemState ->
                        val dialogView = LayoutInflater.from(passThroughContext).inflate(R.layout.item_state_custom_alert, null)
                        redText = dialogView.findViewById(R.id.red_Text)
                        yellowText = dialogView.findViewById(R.id.yellow_Text)
                        greenText = dialogView.findViewById(R.id.green_Text)

                        redImage = dialogView.findViewById(R.id.redImage)
                        yellowImage = dialogView.findViewById(R.id.yellowImage)
                        greenImage = dialogView.findViewById(R.id.greenImage)

                        val count = itemState.optToBuy.count()
                        val name = itemState.fullName
                        yellowText.text = "Nobody is thinking of purchasing this item for $name"
                        if (itemState.optToBuy.count() == 1) {
                            yellowText.text = "$count person is thinking of purchasing this item for $name"
                        } else if (itemState.optToBuy.count() > 1) {
                            yellowText.text = "$count persons are thinking of purchasing this item for $name"
                        }

                        redText.text = "Be the first to purchase this item for  $name"
                        greenText.text = "This item is still available for purchase"

                        if (itemState.itemPurchasedBy.count() == 1) {
                            redText.text = "Item already purchased for $name"
                            greenText.text = "This item is no longer available for purchase"
                        }

                        if (itemState.uid == FirebaseAuth.getInstance().currentUser!!.uid) {
                            redText.visibility = View.GONE
                            greenText.visibility = View.GONE
                            redImage.visibility = View.GONE
                            yellowImage.visibility = View.GONE
                            greenImage.visibility = View.GONE

                            yellowText.layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                            yellowText.text = "Ooops! psych, Sorry it's a surprise"
                        }

                        AlertDialog.Builder(passThroughContext)
                                .setView(dialogView)
                                .show()

                }, { moreButton ->
                    mPost = post
                    posts = moreButton
                    mBottomSheetDialog.setContentView(sheetView)
                    mBottomSheetDialog.setCancelable(true)
                    mBottomSheetDialog.setCanceledOnTouchOutside(true)
                    mBottomSheetDialog.show()
                })
            }
        }

        mBottomSheetDialog.setOnShowListener {
            share = sheetView.findViewById<View>(R.id.share)
            share.setOnClickListener {

                val message = posts.fullName
                val screenshotUri = Uri.parse(posts.postUrl[0])
                val share = Intent(Intent.ACTION_SEND)
                share.type = "text/plain"
                share.type = "image/png"
                share.putExtra(Intent.EXTRA_TEXT, message)
                share.putExtra(Intent.EXTRA_STREAM, screenshotUri)
                startActivity(Intent.createChooser(share, "Title of the dialog the system will open"))

                //val sharingIntent = Intent(Intent.ACTION_SEND)
                //val screenshotUri = Uri.parse(posts.postUrl[0])
                //share.type = "image/png"
                //share.putExtra(Intent.EXTRA_STREAM, screenshotUri)
                //startActivity(Intent.createChooser(share, "Share image using"))
                mBottomSheetDialog.dismiss()
            }

            edit = sheetView.findViewById<View>(R.id.edit)
            edit.setOnClickListener {
                var isEditClicked = true
                val intent = Intent(activity, UploadContentActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable("editpost", posts)
                bundle.putSerializable("isEditClicked", isEditClicked)
                intent.putExtras(bundle)
                startActivity(intent)
                mBottomSheetDialog.dismiss()
            }

            delete = sheetView.findViewById<View>(R.id.delete)
            delete.setOnClickListener {
                mFirestore.collection("posts").document(posts.postId).delete().addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        for(image in posts.postUrl) {
                            val imgRef = mFirebaseStorage.getReferenceFromUrl(image)
                            imgRef.delete().addOnCompleteListener { result ->
                                if (result.isSuccessful) {


//                                    mPost.removeAt()
//                                    recyclerView.adapter.notifyDataSetChanged()
//                                    recyclerView.removeView()


                                } else {

                                }
                            }
                        }
                    } else {

                    }
                }

                mBottomSheetDialog.dismiss()
            }
        }

        mBottomSheetDialog.setOnDismissListener {
            if (sheetView.parent != null) {
                var parent = sheetView.parent as ViewGroup
                parent.removeView(sheetView)
            }
        }

        recyclerView.layoutManager = LinearLayoutManager(passThroughContext)
        return rootView
    }


    companion object {
        val ARG_LIST_TYPE = "LIST_TYPE"
        fun newInstance(listType: PostListType, context: Context): HomeFragment {
            val fragment = HomeFragment(context)
            val args = Bundle()
            args.putSerializable(ARG_LIST_TYPE, listType)
            fragment.arguments = args
            return fragment
        }
    }
}
