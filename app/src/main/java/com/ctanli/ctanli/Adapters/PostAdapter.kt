package com.ctanli.ctanli.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.ctanli.ctanli.Models.Post.Post
import com.ctanli.ctanli.Models.Post.PostHolder
import com.ctanli.ctanli.R

class PostAdapter(private val post: ArrayList<Post>,
                  private val listener: (Post) -> Unit,
                  private val itemStateListener: (Post) -> Unit,
                  private val moreListener: (Post) -> Unit) :
        RecyclerView.Adapter<PostHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder {
        val postItem = LayoutInflater.from(parent.context).inflate(R.layout.post_item, parent, false)
        return PostHolder(postItem)
    }

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        holder.updateWithPuppy(post[position], listener, itemStateListener, moreListener)
    }

    override fun getItemCount(): Int {
        return post.count()
    }

    fun removeAt(position: Int) {
        post[position]
        post.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, post.size)
    }
}