package com.ctanli.ctanli.Adapters

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.ctanli.ctanli.Fragments.ExploreFragment
import com.ctanli.ctanli.Fragments.HomeFragment
import com.ctanli.ctanli.Models.Post.PostListType

class PageAdapter(fm: FragmentManager, private val context: Context) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> return HomeFragment.newInstance(PostListType.Home, context)
            1 -> return ExploreFragment.newInstance(PostListType.Explore, context)
//            2 -> return HomeFragment.newInstance(PostListType.Small, context)
//            3 -> return HomeFragment.newInstance(PostListType.LeashTrained, context)
//            4 -> return HomeFragment.newInstance(PostListType.Active, context)
        }
        return null //HomeFragment.newInstance(PostListType.All, context)
    }

    override fun getCount(): Int {
        // Show 5 total pages.
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        // return empty to show no title. Delete this line to show tab titles
        return null
        /* switch (position) {
            case 0:
                return "All";
            case 1:
                return "Big";
            case 2:
                return "Small";
            case 3:
                return "Trained";
            case 4:
                return "Active";
        }
        return null;*/
    }
}