package com.ctanli.ctanli.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.ctanli.ctanli.Models.Explore.ExplorePostHolder
import com.ctanli.ctanli.Models.Post.Post
import com.ctanli.ctanli.R

class ExploreAdapter(private val post: ArrayList<Post>) : RecyclerView.Adapter<ExplorePostHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExplorePostHolder {
        val postItem = LayoutInflater.from(parent.context).inflate(R.layout.explore_post, parent, false)
        return ExplorePostHolder(postItem)
    }

    override fun onBindViewHolder(holder: ExplorePostHolder, position: Int) {
        holder.updateWithPuppy(post[position])
    }

    override fun getItemCount(): Int {
        return post.count()
    }
}