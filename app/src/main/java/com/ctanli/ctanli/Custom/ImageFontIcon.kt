package com.ctanli.ctanli.Custom

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.Gravity
import android.widget.TextView

class IconSolidTextView : TextView {

    companion object {
        var context: Context? = null
    }

    constructor(context: Context) : super(context) {
        Companion.context = context
        createView()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        Companion.context = context
        createView()
    }

    private fun createView() {
        gravity = Gravity.CENTER
        typeface = Typeface.createFromAsset(Companion.context!!.assets,"fa-solid-900.ttf")
    }
}

class IconRegularTextView : TextView {

    companion object {
        var context: Context? = null
    }

    constructor(context: Context) : super(context) {
        Companion.context = context
        createView()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        Companion.context = context
        createView()
    }

    private fun createView() {
        gravity = Gravity.CENTER
        typeface = Typeface.createFromAsset(Companion.context!!.assets,"fa-regular-400.ttf")
    }
}