package com.ctanli.ctanli.Custom

class TimeClass {

    private val SECOND_MILLIS = 1000
    private val MINUTE_MILLIS = 60 * SECOND_MILLIS
    private val HOUR_MILLIS = 60 * MINUTE_MILLIS
    private val DAY_MILLIS = 24 * HOUR_MILLIS
    private val WEEK_MILLIS = 7 * DAY_MILLIS

    fun getTimeAgo(time: Long): String? {
        var time = time
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000
        }

        val formatted = System.currentTimeMillis()
        if (time > formatted || time <= 0) {
            return null
        }

        // TODO: localize
        val diff = formatted - time
        return when {
            diff < MINUTE_MILLIS -> {
                "just now"
            }
            diff < 2 * MINUTE_MILLIS -> {
                "a minute ago"
            }
            diff < 50 * MINUTE_MILLIS -> {
                val d = diff / MINUTE_MILLIS
                "$d  minutes ago"
            }
            diff < 90 * MINUTE_MILLIS -> {
                "an hour ago"
            }
            diff < 24 * HOUR_MILLIS -> {
                val d = diff / HOUR_MILLIS
                "$d  hours ago"
            }
            diff < 48 * HOUR_MILLIS -> {
                "yesterday"
            }
            diff < 7 * DAY_MILLIS -> {
                val d = diff / DAY_MILLIS
                "$d  days ago"
            }
            diff < 2 * WEEK_MILLIS -> {
                "a week ago"
            }
            else -> {
                val d = diff / WEEK_MILLIS
                "$d  weeks ago"
            }
        }
    }
}
