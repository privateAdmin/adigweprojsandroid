package com.ctanli.ctanli.Custom

import com.ctanli.ctanli.Models.Post.Post
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import java.util.*

class LikePost {

    private val mFirestore = FirebaseFirestore.getInstance()
    private val TAG = "StorageActivity"
    private var likeIsChecked: Boolean? = null
    private var unlikeIsChecked: Boolean? = null

    fun likePost(post: Post, callback: (String) -> Unit) {
        val like = HashMap<String, Any>()
        var myList: MutableList<String> = mutableListOf<String>()
        myList.add((FirebaseAuth.getInstance().currentUser!!.uid))

        likeIsChecked = false
        mFirestore.collection("posts").document(post.postId).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val value = task.result.toObject(Post::class.java)
                for (like in value.likedPost) {
                    myList.add(like)
                    likeIsChecked = true
                }

                if (value.likedPost.isEmpty()) {
                    likeIsChecked = true
                }

                if (likeIsChecked == true) {
                    like["likedPost"] = myList
                    likeIsChecked = false

                    mFirestore.collection("posts").document(post.postId).set(like as Map<String, Any>, SetOptions.merge()).addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            callback(myList.count().toString())
                            updateLikeCount(post.postId, myList.count().toString())
                        }
                    }
                }
            } else {
            }
        }
    }

    fun unLikPost(post: Post, callback: (String) -> Unit) {
        val like = HashMap<String, Any>()
        var myList: MutableList<String> = mutableListOf<String>()

        unlikeIsChecked = false
        mFirestore.collection("posts").document(post.postId).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val value = task.result.toObject(Post::class.java)
                for (like in value.likedPost) {
                    myList.add(like)
                    unlikeIsChecked = true
                }

                if (unlikeIsChecked == true) {
                    like["likedPost"] = myList
                    myList.remove((FirebaseAuth.getInstance().currentUser!!.uid))
                    unlikeIsChecked = false
                    mFirestore.collection("posts").document(post.postId).set(like as Map<String, Any>, SetOptions.merge()).addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            callback(myList.count().toString())
                            updateLikeCount(post.postId, myList.count().toString())
                        }
                    }
                }
            } else {
            }
        }
    }

    fun updateLikeCount(postId: String, likeCounts: String) {
        val likeCount = HashMap<String, Any>()
        likeCount["likeCount"] = likeCounts
        mFirestore.collection("posts").document(postId).update(likeCount).addOnCompleteListener { task ->
            if (task.isSuccessful) {

            } else {

            }
        }
    }
}